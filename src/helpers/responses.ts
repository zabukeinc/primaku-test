export interface ResponseEntity {
  status: number;
  data: any;
  error: any;
}

export class Responses {
  json(status: number, data = null, error = null): ResponseEntity {
    return { status, data, error };
  }
}
