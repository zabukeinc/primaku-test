/** Must be contain symbol, lower case, upper case, symbol and number.  */
export const REGEX_PASSWORD_CHARACTER =
  '^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[-+_!@#$%^&*.,?]).+$';
