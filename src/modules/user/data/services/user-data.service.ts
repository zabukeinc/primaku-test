import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import {
  IPaginationOptions,
  paginate,
  Pagination,
} from 'nestjs-typeorm-paginate';
import { Not, Repository } from 'typeorm';
import { UserEntity, UserRole } from '../../domain/entities/user.entity';
import { hashPassword } from '../../domain/usecases/helpers/auth-helper';
import { UserModel } from '../models/user.model';

@Injectable()
export class UserDataService {
  constructor(
    @InjectRepository(UserModel, 'USER_CONNECTION')
    private readonly repository: Repository<UserModel>,
  ) {}

  async index(options: IPaginationOptions): Promise<Pagination<UserEntity>> {
    return paginate<UserEntity>(this.repository, options);
  }

  async findOne(id: string): Promise<UserEntity> {
    return await this.repository.findOne({
      where: { id },
    });
  }

  async findOneOrFail(id: string): Promise<UserEntity> {
    return await this.repository.findOneOrFail({ where: { id } });
  }

  async findOneByEmail(email: string, id?: string): Promise<UserEntity> {
    const whereExpression = {};

    if (id) {
      Object.assign(whereExpression, { id: Not(id) });
    }

    return await this.repository.findOne({
      where: {
        ...whereExpression,
        email,
      },
    });
  }

  async save(entity: UserEntity): Promise<UserEntity> {
    return await this.repository.save(entity);
  }

  async update(id: string, entity: Partial<UserEntity>): Promise<UserEntity> {
    const updated = await this.repository.update(id, entity);

    if (!updated.affected)
      throw new BadRequestException('Failed to update data');

    return await this.findOne(id);
  }

  async updatePassword(id: string, password: string): Promise<UserEntity> {
    const updated = await this.repository.update(id, {
      password: await hashPassword(password),
    });

    if (!updated.affected)
      throw new BadRequestException('Failed to update password');

    return await this.findOne(id);
  }

  async delete(id: string): Promise<boolean> {
    return (await this.repository.delete(id)).affected > 0;
  }

  async onModuleInit(): Promise<void> {
    if ((await this.repository.count()) < 1) {
      await this.repository.save({
        email: 'admin@primaku.com',
        name: 'Admin Primaku',
        password: await hashPassword('#Admin123'),
        role: UserRole.ADMIN,
      });
    }
  }
}
