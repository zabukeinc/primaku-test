import { Injectable, UnauthorizedException } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { UserDataService } from './data/services/user-data.service';
import { JwtCredentialUserEntity } from './domain/entities/auth/jwt-credential-user.entity';
import { UserEntity } from './domain/entities/user.entity';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(
    private service: UserDataService,
    private configService: ConfigService,
  ) {
    super({
      secretOrKey: configService.get<string>('JWT_SECRET'),
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    });
  }

  async validate(payload: JwtCredentialUserEntity): Promise<UserEntity> {
    if (!payload) throw new UnauthorizedException();

    const user = await this.service.findOne(payload.id);

    if (!user) throw new UnauthorizedException();

    return user;
  }
}
