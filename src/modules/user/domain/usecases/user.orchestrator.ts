import { BadRequestException, Injectable } from '@nestjs/common';
import { Pagination } from 'nestjs-typeorm-paginate';
import { UserDataService } from '../../data/services/user-data.service';
import { IndexUserDTO } from '../../producer/dto/index-user.dto';
import { UpdatePasswordUserDTO } from '../../producer/dto/update-password-user.dto';
import { LoginUserEntity } from '../entities/auth/login-user.entity';
import { ResponseLoginUserEntity } from '../entities/auth/response-login-user.entity';
import { UserEntity } from '../entities/user.entity';
import { LoginUserManager } from './managers/auth/login-user.manager';
import { RegisterUserManager } from './managers/auth/register-user.manager';
import { DeleteUserManager } from './managers/delete-user.manager';
import { UpdatePasswordUserManager } from './managers/update-password-user.manager';
import { UpdateUserManager } from './managers/update-user.manager';

@Injectable()
export class UserOrchestrator {
  constructor(private service: UserDataService) {}

  async index(params: IndexUserDTO): Promise<Pagination<UserEntity>> {
    return await this.service.index({
      page: params.page,
      limit: params.limit,
      route: 'users',
    });
  }

  async show(id: string): Promise<UserEntity> {
    return await this.service.findOne(id);
  }

  async login(body: LoginUserEntity): Promise<ResponseLoginUserEntity> {
    return await new LoginUserManager(this.service, body).execute();
  }

  async register(entity: UserEntity): Promise<UserEntity> {
    return await new RegisterUserManager(this.service, entity).execute();
  }

  async update(
    id: string,
    entity: Partial<UserEntity>,
    headers: string,
  ): Promise<UserEntity> {
    return await new UpdateUserManager(
      this.service,
      id,
      entity,
      headers,
    ).execute();
  }

  async updatePassword(
    body: UpdatePasswordUserDTO,
    headers: string,
  ): Promise<UserEntity> {
    return await new UpdatePasswordUserManager(
      this.service,
      body,
      headers,
    ).execute();
  }

  async delete(
    id: string,
    headers: string,
  ): Promise<void | BadRequestException> {
    return await new DeleteUserManager(this.service, id, headers).execute();
  }
}
