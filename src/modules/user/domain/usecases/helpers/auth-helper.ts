import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcrypt';
import { JwtCredentialUserEntity } from '../../entities/auth/jwt-credential-user.entity';
import { UserEntity, UserRole } from '../../entities/user.entity';

/**
 * Transform plain text password to hashed password.
 * @param plainTextPassword
 * @param saltRound
 * @returns {string}
 */
export const hashPassword = async (
  plainTextPassword: string,
  saltRound = 10,
): Promise<string> => {
  return await bcrypt.hash(plainTextPassword, saltRound);
};

/**
 * Comparing plain text password to hashed password.
 * @param plainTextPassword
 * @param hashedPassword
 * @returns Result (match or not match) plain text password and hashed password.
 */
export const comparePassword = async (
  plainTextPassword: string,
  hashedPassword: string,
): Promise<boolean> => {
  return await bcrypt.compare(plainTextPassword, hashedPassword);
};

/**
 * Generate token jwt
 * @param param0 id {string}
 * @returns
 */
export const generateToken = (user: UserEntity): string => {
  const credentials = {
    id: user.id,
    role: user.role,
  } as JwtCredentialUserEntity;

  return new JwtService().sign(credentials, {
    secret: process.env.JWT_SECRET,
  });
};

export const getRole = (headers: string): UserRole => {
  return extractToken(headers)['role'];
};

export const extractToken = (headers: string): JwtCredentialUserEntity => {
  const auth = headers['authorization'] as string;
  const token = auth.substring(7, auth.length - 1);

  return new JwtService().decode(token) as JwtCredentialUserEntity;
};
