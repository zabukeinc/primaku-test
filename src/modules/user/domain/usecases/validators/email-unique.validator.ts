import { BadRequestException } from '@nestjs/common';
import { UserDataService } from 'src/modules/user/data/services/user-data.service';

export class EmailUniqueValidator {
  constructor(
    protected service: UserDataService,
    protected email: string,
    protected id?: string,
  ) {}

  async validate(): Promise<void | BadRequestException> {
    const isExist = await this.service.findOneByEmail(this.email, this.id);

    if (isExist) throw new BadRequestException('Email already exist.');
  }
}
