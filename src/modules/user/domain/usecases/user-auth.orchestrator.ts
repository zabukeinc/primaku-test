import { Injectable } from '@nestjs/common';
import { UserDataService } from '../../data/services/user-data.service';
import { LoginUserEntity } from '../entities/auth/login-user.entity';
import { ResponseLoginUserEntity } from '../entities/auth/response-login-user.entity';
import { UserEntity } from '../entities/user.entity';
import { LoginUserManager } from './managers/auth/login-user.manager';
import { RegisterUserManager } from './managers/auth/register-user.manager';

@Injectable()
export class UserAuthOrchestrator {
  constructor(private service: UserDataService) {}

  async login(body: LoginUserEntity): Promise<ResponseLoginUserEntity> {
    return await new LoginUserManager(this.service, body).execute();
  }

  async register(entity: UserEntity): Promise<UserEntity> {
    return await new RegisterUserManager(this.service, entity).execute();
  }
}
