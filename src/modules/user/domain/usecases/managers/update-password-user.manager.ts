import { UnauthorizedException } from '@nestjs/common';
import { UserDataService } from 'src/modules/user/data/services/user-data.service';
import { UpdatePasswordUserDTO } from 'src/modules/user/producer/dto/update-password-user.dto';
import { UserEntity } from '../../entities/user.entity';
import { extractToken } from '../helpers/auth-helper';

export class UpdatePasswordUserManager {
  constructor(
    protected service: UserDataService,
    protected body: UpdatePasswordUserDTO,
    protected headers: string,
  ) {}

  async execute(): Promise<UserEntity> {
    const credential = extractToken(this.headers);
    if (!credential) throw new UnauthorizedException();
    return await this.service.updatePassword(credential.id, this.body.password);
  }
}
