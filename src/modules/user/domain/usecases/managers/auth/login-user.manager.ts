import { BadRequestException } from '@nestjs/common';
import { UserDataService } from 'src/modules/user/data/services/user-data.service';
import { LoginUserEntity } from '../../../entities/auth/login-user.entity';
import { ResponseLoginUserEntity } from '../../../entities/auth/response-login-user.entity';
import { UserEntity } from '../../../entities/user.entity';
import { comparePassword, generateToken } from '../../helpers/auth-helper';

export class LoginUserManager {
  constructor(
    protected service: UserDataService,
    protected body: LoginUserEntity,
  ) {}

  async execute(): Promise<ResponseLoginUserEntity> {
    const existingUser = await this.service.findOneByEmail(this.body.email);

    if (!existingUser) throw new BadRequestException('User not found');

    if (!(await this.isValidPassword(existingUser)))
      throw new BadRequestException(`Password doesn't match`);

    return {
      user_id: existingUser.id,
      token: generateToken(existingUser),
    };
  }

  protected async isValidPassword(user: UserEntity): Promise<boolean> {
    return await comparePassword(this.body.password, user.password);
  }
}
