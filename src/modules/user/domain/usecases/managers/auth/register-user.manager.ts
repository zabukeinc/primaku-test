import { UserDataService } from 'src/modules/user/data/services/user-data.service';
import { UserEntity } from '../../../entities/user.entity';
import { hashPassword } from '../../helpers/auth-helper';
import { EmailUniqueValidator } from '../../validators/email-unique.validator';

export class RegisterUserManager {
  constructor(
    protected service: UserDataService,
    protected entity: UserEntity,
  ) {}

  async execute(): Promise<UserEntity> {
    await this.prepareData();
    await this.preProcess();

    return await this.service.save(this.entity);
  }

  protected async prepareData(): Promise<void> {
    this.entity.password = await hashPassword(this.entity.password);

    return;
  }

  protected async preProcess(): Promise<void> {
    await new EmailUniqueValidator(this.service, this.entity.email).validate();
    return;
  }
}
