import { BadRequestException } from '@nestjs/common';
import { UserDataService } from 'src/modules/user/data/services/user-data.service';
import { UserRole } from '../../entities/user.entity';
import { getRole } from '../helpers/auth-helper';

export class DeleteUserManager {
  constructor(
    protected service: UserDataService,
    protected id: string,
    protected headers: string,
  ) {}

  async execute(): Promise<void> {
    if (getRole(this.headers) === UserRole.USER)
      throw new BadRequestException('Only admin that allowed to delete data');

    if (!(await this.service.delete(this.id))) {
      throw new BadRequestException('Failed to delete data.');
    }
  }
}
