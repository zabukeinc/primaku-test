import { BadRequestException } from '@nestjs/common';
import { UserDataService } from 'src/modules/user/data/services/user-data.service';
import { UserEntity, UserRole } from '../../entities/user.entity';
import { extractToken } from '../helpers/auth-helper';
import { EmailUniqueValidator } from '../validators/email-unique.validator';

export class UpdateUserManager {
  constructor(
    protected service: UserDataService,
    protected id: string,
    protected entity: Partial<UserEntity>,
    protected headers: string,
  ) {}

  async execute(): Promise<UserEntity> {
    await this.preProcess();

    return await this.service.update(this.id, this.entity);
  }

  protected async preProcess(): Promise<void> {
    if (!(await this.isValidCrendentialToUpdate())) {
      throw new BadRequestException(
        'User role cannot update data with user admin.',
      );
    }

    await new EmailUniqueValidator(
      this.service,
      this.entity.email,
      this.id,
    ).validate();
  }

  protected async isValidCrendentialToUpdate(): Promise<boolean> {
    const currentCredential = extractToken(this.headers);
    const userToUpdate = await this.service.findOne(this.id);

    return (
      userToUpdate.role === UserRole.ADMIN &&
      currentCredential.role === UserRole.USER
    );
  }
}
