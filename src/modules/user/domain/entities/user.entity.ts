export enum UserRole {
  ADMIN = 'admin',
  USER = 'user',
}

export interface UserEntity {
  id: string;
  name: string;
  email: string;
  password: string;
  role: UserRole;
  created_at?: Date;
  updated_at?: Date;
}
