import { UserRole } from '../user.entity';

export interface JwtCredentialUserEntity {
  id: string;
  role: UserRole;
}
