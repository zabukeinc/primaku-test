export interface ResponseLoginUserEntity {
  token: string;
  user_id: string;
}
