export interface LoginUserEntity {
  email: string;
  password: string;
}
