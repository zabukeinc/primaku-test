import {
  BadRequestException,
  Body,
  Controller,
  HttpStatus,
  Post,
} from '@nestjs/common';
import { ResponseEntity, Responses } from 'src/helpers/responses';
import { UserAuthOrchestrator } from '../domain/usecases/user-auth.orchestrator';
import { CreateUserDTO } from './dto/create-user.dto';
import { LoginUserDTO } from './dto/login-user.dto';

@Controller('users')
export class UserAuthController {
  constructor(protected orchestrator: UserAuthOrchestrator) {}

  protected responses = new Responses();

  @Post('login')
  async login(@Body() body: LoginUserDTO): Promise<ResponseEntity> {
    try {
      return this.responses.json(200, await this.orchestrator.login(body));
    } catch (err) {
      throw new BadRequestException(err.message);
    }
  }

  @Post()
  async register(@Body() entity: CreateUserDTO): Promise<ResponseEntity> {
    try {
      return this.responses.json(
        HttpStatus.CREATED,
        await this.orchestrator.register(entity),
      );
    } catch (err) {
      throw new BadRequestException(err.message);
    }
  }
}
