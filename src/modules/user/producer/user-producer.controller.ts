import {
  BadRequestException,
  Body,
  Controller,
  Delete,
  Get,
  Headers,
  HttpStatus,
  Param,
  Patch,
  Put,
  Query,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth } from '@nestjs/swagger';
import { ResponseEntity, Responses } from 'src/helpers/responses';
import { UserOrchestrator } from '../domain/usecases/user.orchestrator';
import { IndexUserDTO } from './dto/index-user.dto';
import { UpdatePasswordUserDTO } from './dto/update-password-user.dto';
import { PatchUserDTO } from './dto/patch-user';

@Controller('users')
@ApiBearerAuth()
@UseGuards(AuthGuard())
export class UserController {
  constructor(protected readonly orchestrator: UserOrchestrator) {}

  protected responses = new Responses();

  @Get()
  async index(@Query() params: IndexUserDTO): Promise<ResponseEntity> {
    try {
      return this.responses.json(
        HttpStatus.OK,
        await this.orchestrator.index(params),
      );
    } catch (err) {
      throw new BadRequestException(err.message);
    }
  }

  @Get(':id')
  async show(@Param('id') id: string): Promise<ResponseEntity> {
    try {
      return this.responses.json(
        HttpStatus.OK,
        await this.orchestrator.show(id),
      );
    } catch (err) {
      throw new BadRequestException(err.message);
    }
  }

  @Patch(':id')
  async update(
    @Param('id') id: string,
    @Body() entity: PatchUserDTO,
    @Headers() headers: string,
  ): Promise<ResponseEntity> {
    try {
      return this.responses.json(
        HttpStatus.OK,
        await this.orchestrator.update(id, entity, headers),
      );
    } catch (err) {
      throw new BadRequestException(err.message);
    }
  }

  @Put('password')
  async updatePassword(
    @Body() body: UpdatePasswordUserDTO,
    @Headers() headers: string,
  ): Promise<ResponseEntity> {
    try {
      return this.responses.json(
        HttpStatus.OK,
        await this.orchestrator.updatePassword(body, headers),
      );
    } catch (err) {
      throw new BadRequestException(err.message);
    }
  }

  @Delete()
  async delete(
    @Query('id') id: string,
    @Headers() headers: string,
  ): Promise<ResponseEntity> {
    try {
      return this.responses.json(
        HttpStatus.NO_CONTENT,
        await this.orchestrator.delete(id, headers),
      );
    } catch (err) {
      throw new BadRequestException(err.message);
    }
  }
}
