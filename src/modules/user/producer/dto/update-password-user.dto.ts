import { ApiProperty } from '@nestjs/swagger';
import { MinLength, Matches } from 'class-validator';
import { REGEX_PASSWORD_CHARACTER } from 'src/helpers/regex';

export class UpdatePasswordUserDTO {
  @ApiProperty({ type: 'string', required: true })
  @MinLength(8)
  @Matches(REGEX_PASSWORD_CHARACTER, '', {
    message:
      'password must be contain mix of uppercase, lowercase, symbol and number',
  })
  password: string;
}
