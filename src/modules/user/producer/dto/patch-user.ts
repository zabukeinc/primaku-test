import { ApiProperty } from '@nestjs/swagger';
import { MaxLength, IsEmail, IsIn } from 'class-validator';
import { UserEntity, UserRole } from '../../domain/entities/user.entity';

export class PatchUserDTO implements Partial<UserEntity> {
  @ApiProperty({ type: 'string', required: true })
  @MaxLength(50)
  name: string;

  @ApiProperty({ type: 'string', required: true })
  @IsEmail()
  email: string;

  @ApiProperty({ type: 'enum', enum: UserRole })
  @IsIn(Object.values(UserRole))
  role: UserRole;
}
