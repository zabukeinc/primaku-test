import { ApiProperty } from '@nestjs/swagger';
import { Matches, MinLength } from 'class-validator';
import { REGEX_PASSWORD_CHARACTER } from 'src/helpers/regex';
import { LoginUserEntity } from 'src/modules/user/domain/entities/auth/login-user.entity';

export class LoginUserDTO implements LoginUserEntity {
  @ApiProperty({ type: 'string', required: true })
  email: string;

  @ApiProperty({ type: 'string', required: true })
  @MinLength(8, {
    message: 'Email or password is invalid',
  })
  @Matches(REGEX_PASSWORD_CHARACTER, '', {
    message: 'Email or password is invalid',
  })
  password: string;
}
