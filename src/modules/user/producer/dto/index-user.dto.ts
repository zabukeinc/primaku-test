import { ApiProperty } from '@nestjs/swagger';

export class IndexUserDTO {
  @ApiProperty({
    type: 'number',
    required: true,
    description: 'Page of the data',
  })
  page: number;

  @ApiProperty({
    type: 'number',
    required: true,
    description: 'Limit of the data',
  })
  limit: number;
}
