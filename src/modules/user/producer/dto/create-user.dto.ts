import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsIn, Matches, MaxLength, MinLength } from 'class-validator';
import { REGEX_PASSWORD_CHARACTER } from 'src/helpers/regex';
import { UserEntity, UserRole } from '../../domain/entities/user.entity';

export class CreateUserDTO implements UserEntity {
  id: string;
  @ApiProperty({ type: 'string', required: true })
  @MaxLength(50)
  name: string;

  @ApiProperty({ type: 'string', required: true })
  @IsEmail()
  email: string;

  @ApiProperty({ type: 'string', required: true })
  @MinLength(8)
  @Matches(REGEX_PASSWORD_CHARACTER, '', {
    message:
      'password must be contain mix of uppercase, lowercase, symbol and number',
  })
  password: string;

  @ApiProperty({ type: 'enum', enum: UserRole, required: true })
  @IsIn(Object.values(UserRole))
  role: UserRole;

  created_at?: Date;
  updated_at?: Date;
}
