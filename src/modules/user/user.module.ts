import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserModel } from './data/models/user.model';
import { UserDataService } from './data/services/user-data.service';
import { UserOrchestrator } from './domain/usecases/user.orchestrator';
import { UserController } from './producer/user-producer.controller';
import * as dotenv from 'dotenv';
import { PassportModule } from '@nestjs/passport';
import { JwtStrategy } from './jwt-strategy';
import { UserAuthController } from './producer/user-auth-producer.controller';
import { UserAuthOrchestrator } from './domain/usecases/user-auth.orchestrator';
dotenv.config();
@Module({
  imports: [
    PassportModule.register({ defaultStrategy: 'jwt' }),
    JwtModule.register({
      secret: process.env.JWT_SECRET,
      signOptions: {
        expiresIn: process.env.JWT_EXPIRE_TIME,
      },
    }),

    TypeOrmModule.forRootAsync({
      name: 'USER_CONNECTION',
      useFactory: () => {
        return {
          type: 'mysql',
          host: process.env.MYSQL_HOST,
          port: parseInt(process.env.MYSQL_PORT),
          username: process.env.MYSQL_USERNAME,
          password: process.env.MYSQL_PASSWORD,
          database: process.env.MYSQL_DB,
          entities: [UserModel],
          synchronize: true,
        };
      },
    }),
    TypeOrmModule.forFeature([UserModel], 'USER_CONNECTION'),
  ],
  controllers: [UserController, UserAuthController],
  providers: [
    UserDataService,
    UserOrchestrator,
    UserAuthOrchestrator,
    JwtStrategy,
  ],
  exports: [JwtStrategy, PassportModule, JwtModule, UserDataService],
})
export class UserModule {}
