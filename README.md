## Description

[Nest](https://github.com/nestjs/nest) framework TypeScript starter repository.

## Installation

```bash
$ npm install

```

## Running the app

```bash
1. create database db_primaku or adjust env file with your db config machine
2. npm install
3. npm run start:dev/npm run start/npm run start:prod
4. access to localhost:3000/api
```